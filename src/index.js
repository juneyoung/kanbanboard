import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './your_style.css';
// import App from './App';
import registerServiceWorker from './registerServiceWorker';

/*
	MD 를 표현하기 위해 
	- npm install --save marked
*/
import KanbanBoard from './components/KanbanBoard'


/*
	React 컴포넌트에 ref 속성을 부여하게 되면 this.refs 로 접근할 수 있음 (DOM 에 자체 접근 )
*/

let cardList = [
	{
		id : 1
		, title : 'Read the book'
		, description : 'a'
		, status : 'in-progress'
		, color : '#BD8D31'
		, tasks : [
			{id:1, name: 'Task Doing 1', done:true}
			, {id:2, name: 'Task Doing 2', done:false}
			, {id:3, name: 'Task Doing 3', done:false}
		]
	}
	, {
		id : 2
		, title : 'Write some code'
		, description : 'Complete source code in [github](https://github.com/pro-react)'
		, status : 'todo'
		, color : '#3A7E28'
		, tasks : [
			{id:1, name: 'Task Todo 1', done:true}
			, {id:2, name: 'Task Todo 2', done:false}
			, {id:3, name: 'Task Todo 3', done:false}
		]
	}
];

let rootElement = document.getElementById('root');

ReactDOM.render(
	<KanbanBoard cards={cardList} />
	, rootElement
);
registerServiceWorker();
