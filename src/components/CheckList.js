import React from 'react'
import PropTypes from 'prop-types'

export default class CheckList extends React.Component {

	render () {

		//List.js 에서는 cards 에 대해서 return 을 하는데 여기는 왜 return 이 없는지 
		//return 안하면 안됨 
		console.log('CheckList', this.props);

		let tasks = this.props.tasks.map((task) => {
			return  <li className='checklist_task' key={task.id}>
						<input type='checkbox' defaultChecked={task.done} />
						{task.name}
						<a href='#' className='checklist_task--remove'></a>
					</li>;
		});

		return (
			<div className='checklist'>
				<ul>{tasks}</ul>

				<input type='text'
					className='checklist--add-task'
					placeholder='Type then hit Enter to add a task'/>
			</div>
		);
	}
}

CheckList.propTypes = {
	cardId : PropTypes.string
	, tasks : PropTypes.arrayOf(PropTypes.object)
}