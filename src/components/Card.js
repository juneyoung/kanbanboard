import React from 'react';
import CheckList from './CheckList';
import marked from 'marked';
import PropTypes from 'prop-types';


/*
	커스텀 props 유효성 검사
*/

let titlePropType = (props, propName, componentName) => {
	if(props[propName]) {
		let value = props[propName];

		/*
			ES6 에서는 템플릿 문자열을 표현할 때 백틱-백쿼트- 를 사용해야 함
			더블쿼트나 쿼트를 사용할 경우에는 :: Unexpected template string expression no-template-curly-in-string :: 경고 발생 
		*/
		if(typeof value !== 'string' || value.length > 80) {
			return new Error(
				`${propName} in ${componentName} is longer than 80 characters`
			);
		}
	}
}


export default class Card extends React.Component {

	constructor () {
		super();
		// super(...arguments); //arguments 를 안받는데 super 로 왜 보내나.
		this.state = {
			showDetails : false
		}
	}

	toggleDetails () {
		this.setState({
			showDetails : !this.state.showDetails
		})
	}

	render () {

		let cardDetails;
		if(this.state.showDetails) {
			cardDetails = (
				/*
				<div className='card_details'>
					{marked(this.props.description)}
					<CheckList cardId={this.props.id} tasks={this.props.tasks}/>
				</div>
				*/

				<div className='card_details'>
					 <span dangerouslySetInnerHTML={{__html: marked(this.props.description)}} />
					<CheckList cardId={this.props.id} tasks={this.props.tasks}/>
				</div>
			)
		}

		let sideColor = {
			position : 'absolute'
			, zIndex : -1
			, top : 0
			, bottom : 0
			, left : 0
			, width : 7
			, backgroundColor : this.props.color
		}

		/*
			이벤트 핸들러 주의사항
			- onClick={setState()} : 무한루프의 원인이 됨. 화살표 펑션 사용하기
			- onClick={() => this.toggleDetails} : 당연하게 할당만 되고 실행 안됨
			- onClick={() => toggleDetails() } : toggleDetails is not defined

			this 를 못찾을 경우 bind 펑션을 사용함
			- onClick={this.toggleDetails.bind(this)}
		*/
		let toggleClassName = 'card_title';
		return (
			<div className='card'>
				<div style={sideColor}/>
				<div className={this.state.showDetails ? toggleClassName + ' card_title--is-open' : toggleClassName}
					onClick={() => this.toggleDetails()}>
					{this.props.title}
				</div>
				{cardDetails}
			</div>
		);
	}
}

Card.propTypes = {
	id : PropTypes.number
	, title : titlePropType
	, description : PropTypes.string
	, color : PropTypes.string
	, tasks : PropTypes.arrayOf(PropTypes.object)
}