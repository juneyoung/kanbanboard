import React from 'react';
import Card from './Card';
import PropTypes from 'prop-types'

export default class List extends React.Component {

	render () {

		let cards = this.props.cards.map((card) => {
			// return 다음에 줄 넘겨쓰면 인식 못함 
			return  <Card id={card.id}
						key={card.id}
						title={card.title}
						color={card.color}
						description={card.description}
						tasks={card.tasks}/>;
		});

		return (
			<div className='list'>
				<h1>{this.props.title}</h1>
				{cards}	
			</div>
		);
	}
}

List.propTypes = {
	title : PropTypes.string.isRequired
	, cards : PropTypes.arrayOf(PropTypes.object)
}