import React from 'react';
import PropTypes from 'prop-types';
import List from './List'

/*
// PropTypes 를 메인에서 가져오는 방식은 v15 부터 deprecated
import React, { PropTypes } from 'react';
*/

export default class KanbanBoard extends React.Component {

	constructor () { 
		/*
			- constructor 에 반드시 super() 로 시작해야 함
			- super() 를 호출하지 않은 경우에는 여기에 this 스테이트먼트를 사용할 수 없다는 메세지 출력됨  
			- 여기에서 state 를 명시하지 않으면 this.state 를 참조할 수 없다(자체가 내장은 아니다)
		*/

		super();

		this.state = {
			fileName : 'KanbanBoard'
		}
	}

	render () {
		// {this.state.fileName}
		return (
			<div className='app'>
			 	<List id='todo' title='To Do' 
			 		cards={this.props.cards.filter((card) => card.status === 'todo')}
		 		/>

			 	<List id='in-progress' title='In Progress' 
			 		cards={this.props.cards.filter((card) => card.status === 'in-progress')}
		 		/>

			 	<List id='done' title='Done' 
			 		cards={this.props.cards.filter((card) => card.status === 'done')}
		 		/>
			</div>
		);
	}
}

KanbanBoard.propTypes = {
	cards : PropTypes.arrayOf(PropTypes.object)
}