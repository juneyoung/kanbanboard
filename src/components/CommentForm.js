import React from 'react';

let { form, input } = React.DOM;
// let Factory = React.createFactory(ComponentClass);

export default class CommentForm extends React.Component {
	render () {
		/*
		리액트 엘리먼트 생성법 
			React.createElement('input', {type: 'text'}, ...);
			React.DOM.input({type: 'text'})

		Component 의 value 와 defaultValue 를 구분
			value : React 제어 컴포넌트의 값 			// 사용자 입력 영향 X
			defaultValue : React 비제어 컴포넌트의 값 	// 사용자 입력 영향 O
		*/

		return form({className : 'commentForm'}
			,input({type: 'text', placeHolder: 'name'})
			,input({type: 'text', placeHolder: 'comment'})
			,input({type: 'submit', value: 'Post'})
		);
	}
}